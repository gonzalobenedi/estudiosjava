/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Model.Study;
import Persistance.StudyDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class StudyController extends BaseController {

    private static final Logger LOG = Logger.getLogger(StudyController.class.getName());
        private StudyDAO studyDAO;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet StudyController</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet StudyController at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
//    }
    public void index(){
        LOG.info("En StudyController->index()");
        StudyDAO dao = new StudyDAO();
        ArrayList<Study> studies = new ArrayList();
        studies = dao.getAll();
        request.setAttribute("studies", studies);
        LOG.info("En StudyController->index()");
        dispatch("/WEB-INF/view/study/index.jsp");
    }
    
    public void create(){
        dispatch("/WEB-INF/view/study/create.jsp");
    }
    
    public void insert() 
{
    studyDAO = new StudyDAO();
    LOG.info("crear DAO");
    
    Study study = new Study();
    LOG.info("Crear modelo");
    study.setCode(request.getParameter("code"));
    study.setName(request.getParameter("name"));
    study.setShortName(request.getParameter("shortName"));
    study.setAbreviation(request.getParameter("abreviation"));
    LOG.info("Datos cargados");
    synchronized (studyDAO) {
        try {
            studyDAO.insert(study);
        } catch (SQLException ex) {
            Logger.getLogger(StudyController.class.getName()).log(Level.SEVERE, null, ex);
            request.setAttribute("ex", ex);
            request.setAttribute("msg", "Error de base de datos ");
            dispatch("/WEB-INF/view/error/index.jsp");
        }
    }
    redirect(contextPath + "/study/index");
}
    
    public void edit() {
        
        dispatch("/WEB-INF/view/study/edit.jsp");
    }
    
    public void update() {
        //Recibe los datos del formulario, los actuliza en la BBDD y redirige a index
    }
    
    public void delete() {
        //Elimina el estudio de la BBDD
    }
    
    public void search() {
        //Redirige al formulario de busqueda por nombre
    }
    
    public void find() {
        //Recibe los datos del formulario, busca en la BBDD y los muestra en index
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
