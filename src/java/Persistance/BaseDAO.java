/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class BaseDAO {

    private static final Logger LOG = Logger.getLogger(BaseDAO.class.getName());
    
    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://127.0.0.1/matricula";
//    public static final String DB_URL = "jdbc:mysql://10.2.25.38/matricula";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "root";    
    public Connection connection;

    public void connect()
    {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            LOG.info("Conectado");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void disconnect()
    {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
