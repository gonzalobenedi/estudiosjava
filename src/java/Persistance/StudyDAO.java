/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistance;

import Model.Study;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class StudyDAO extends BaseDAO{

    private static final Logger LOG = Logger.getLogger(StudyDAO.class.getName());
    
    
    
    public ArrayList<Study> getAll(){
        connect();
        PreparedStatement stmt = null;
        ArrayList<Study> studies = null;

        try {
            stmt = connection.prepareStatement("select * from studies");
            ResultSet rs = stmt.executeQuery();
            studies = new ArrayList();
            //stmt.close();
            //disconnect();
            int i = 0;
            while (rs.next()) {
                i++;
                Study study = new Study();
                study.setId(rs.getLong("id"));
                study.setCode(rs.getString("code"));
                study.setName(rs.getString("name"));
                study.setShortName(rs.getString("shortName"));
                study.setAbreviation(rs.getString("abreviation"));

                studies.add(study);
                LOG.info("Registro fila: " + i);
            }
            return studies;
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Study get(int id){
        connect();
        Study study = new Study();
        PreparedStatement stmt = null;
        
        try {
            stmt = connection.prepareStatement("select * from studies where id=?");
            ResultSet rs = stmt.executeQuery();
            disconnect();
            study.setId(rs.getLong("id"));
            study.setCode(rs .getString("code"));
            study.setName(rs .getString("name"));
            study.setShortName(rs .getString("shortName"));
            study.setAbreviation(rs .getString("abreviation"));
            
            return study;
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void insert(Study study) throws SQLException {
        connect();
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO studies(code, name, shortName, abreviation)"
                + " VALUES(?, ?, ?, ?)"
        );
        stmt.setString(1, study.getCode());
        stmt.setString(2, study.getName());
        stmt.setString(3, study.getShortName());
        stmt.setString(4, study.getAbreviation());

        stmt.execute();
        disconnect();
}
    
    public void update(Study study){
        connect();
        PreparedStatement stmt = null;
        
        try {
            stmt = connection.prepareStatement("update studies set code=?, name=?, shortName=?, abreviation=? where id=?");
            stmt.setString(1, study.getCode());
            stmt.setString(2, study.getName());
            stmt.setString(3, study.getShortName());
            stmt.setString(4, study.getAbreviation());
            stmt.setLong(5, study.getId());
            
            stmt.executeUpdate();
            disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void delete(int id){
        connect();
        PreparedStatement stmt = null;
        
        try {
            stmt = connection.prepareStatement("delete from studies where id=?");
            stmt.setInt(1,id);
            
            stmt.executeUpdate();
            disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Study> find(String pattern){
        ArrayList<Study> studies = new ArrayList<>();
        return studies;
    }
}
