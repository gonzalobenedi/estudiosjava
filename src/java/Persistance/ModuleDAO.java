/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistance;

import Model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class ModuleDAO extends BaseDAO{

    private static final Logger LOG = Logger.getLogger(ModuleDAO.class.getName());
    
    public ArrayList<Module> getAll()
    {
        PreparedStatement stmt = null;
        ArrayList<Module> modules = null;

        try {
            stmt = connection.prepareStatement("select * from modules");
            ResultSet rs = stmt.executeQuery();
            modules = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Module module = new Module();
                module.setId(rs.getLong("id"));
                module.setCode(rs.getString("code"));
                module.setName(rs.getString("name"));
                module.setLevel(rs.getString("level"));
                module.setLevel(rs.getString("hoursTotal"));
                module.setLevel(rs.getString("hoursWeekly"));

                modules.add(module);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modules;
    }
    
    public Module get(int id){
        Module module = new Module();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("select * from modules where id=?");
            stmt.setLong(1, module.getId());
            ResultSet rs = stmt.executeQuery();
            
            module.setId(rs.getLong("id"));
            module.setCode(rs.getString("code"));
            module.setName(rs.getString("name"));
            module.setLevel(rs.getString("level"));
            module.setLevel(rs.getString("hoursTotal"));
            module.setLevel(rs.getString("hoursWeekly"));

            return module;
            
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void insert(Module module) throws SQLException {
    PreparedStatement stmt = null;
    this.connect();
    stmt = connection.prepareStatement(
            "INSERT INTO modules(code, name, level, hoursTotal, hoursWeekly)"
            + " VALUES(?, ?, ?, ?, ?)"
    );
    stmt.setString(1, module.getCode());
    stmt.setString(2, module.getName());
    stmt.setString(3, module.getLevel());
    stmt.setString(4, module.gethTotal());
    stmt.setString(5, module.gethWeekly());

    stmt.execute();
}
    
    public void update(Module module){
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("update modules set code=?, name=?, level=?, hoursTotal=?, hoursWeekly=? where id=?");
            
            stmt.setString(1, module.getCode());
            stmt.setString(2, module.getName());
            stmt.setString(3, module.getLevel());
            stmt.setString(4, module.gethTotal());
            stmt.setString(5, module.gethWeekly());
            stmt.setLong(6, module.getId());
            
            stmt.executeUpdate();
        
        } catch (SQLException ex) {
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(int id){
        PreparedStatement stmt = null;
        try{
            stmt = connection.prepareStatement("delete from modules where id=?");
            stmt.setInt(1, id);
            
            stmt.executeUpdate();
        }catch(SQLException ex){
            Logger.getLogger(StudyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public ArrayList<Module> find(String pattern){
        ArrayList<Module> modules = new ArrayList<>();
        return modules;
    }
}
