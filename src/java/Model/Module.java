/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author usuario
 */
public class Module {
    private long id;
    private String code;
    private String name;
    private String level;
    private String hTotal;
    private String hWeekly;

    public Module() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String gethTotal() {
        return hTotal;
    }

    public void sethTotal(String hTotal) {
        this.hTotal = hTotal;
    }

    public String gethWeekly() {
        return hWeekly;
    }

    public void sethWeekly(String hWeekly) {
        this.hWeekly = hWeekly;
    }
    
    

}
