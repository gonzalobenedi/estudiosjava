# Ejercicio Estudios Java2EE#

### Qué es este repositorio? ###

* Este repositorio contiene el ejercicio de estudios planteado en la asignatura de Desarrollo en Entorno Servidor durante el 2º curso del grado de Desarrollo de Aplicaciones Web.
* Versión: x.xx

### Set up? ###

* Haz fork o clona el repositorio en tu máquina, abre el proyecto con NetBeans/Eclipse etc.
* Servidor donde desplegar la aplicación.
* Configuración BBDD: Necesaria una BBDD de MySQL. Cambiar parámetros en el fichero de conexión.
* En NetBeans es necesario tener instalado Glassfish.