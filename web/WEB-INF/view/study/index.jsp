<%-- 
    Document   : hola
    Created on : 09-ene-2017, 18:54:23
    Author     : alumno
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.Study"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@include file="/WEB-INF/view/header.jsp" %>
<h2>Lista de estudios</h2>
  <br>
  <p>
    <a href="<%= request.getContextPath()%>/study/create"><i class="fa fa-plus"></i> A�adir estudio</a>
  </p>
  <br>
  <jsp:useBean id="studies" class="ArrayList<Study>" scope="request"/>  
  <table>
        <tr>
            <th>C�digo</th>
            <th>Abreviatura</th>
            <th>Nombre</th>
            <th>Nombre Corto</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
  <%
        Iterator<Study> iterator = studies.iterator();
        while (iterator.hasNext()) {
            Study study = iterator.next();%>
    <tr>
        <td><%= study.getCode()%></td>
        <td><%= study.getAbreviation()%></td>
        <td><%= study.getName()%></td>
        <td><%= study.getShortName()%></td>
        <td><a href="<%= request.getContextPath()%>/study/get/<%= study.getId()%>"><i class="fa fa-edit"></i></a></td>
        <td><a href="<%= request.getContextPath()%>/study/delete/<%= study.getId()%>"><i class="fa fa-trash"></i></a></td>
    </tr>
    <%
        }
    %>
    </table>
          
<%@include file="/WEB-INF/view/footer.jsp" %>
