// MENU
$(document).ready(main);

var contador = 1;

function main() {
    $('#btn').click(function() {

        if (contador == 1) {
            $('nav').animate({
                left: '0'
            });
            contador = 0;
        } else {
            contador = 1;
            $('nav').animate({
                left: '-100%'
            });
        }

    });
}

// FORMULARIO
function comprobarNombre() {
  if($('#nombre').val().length < 1){
    var p = document.getElementById('enombre');
    p.style.display = "block";
    return false;
  }
  return true;
}

function comprobarEmail() {
  expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if ( !expr.test(document.getElementById('mail').value)){
      var p = document.getElementById('email');
      p.style.display = "block";
      return false;
  }
  return true;
}



function comprobarMensaje() {
  if($('#message').val().length < 1){
    var p = document.getElementById('emensaje');
    p.style.display = "block";
    return false;
  }
  return true;
}

function comprobar() {
  if(!comprobarNombre()||!comprobarEmail()||!comprobarMensaje()){
    return false;
  }
  alert("Formulario corretamente rellenado");
}

// MODAL
var modal = document.getElementById('mymodal');

var btn = document.getElementById("modalBtn");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
    modal.style.display = "block";
};

span.onclick = function() {
    modal.style.display = "none";
};

// FUNCION MODALS
function mostrarModal(modalId, pos) {
  var modal = document.getElementById(modalId);
  var span = document.getElementsByClassName('close')[pos];

  modal.style.display = "block";
  span.onclick = function() {
      modal.style.display = "none";
  };
}
